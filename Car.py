from dataclasses import dataclass
import numpy as np
from enum import Enum


class Direction(Enum):
    FORWARD = 0
    BACKWARD = 1
    UNDEFINED = -1


@dataclass
class Car:
    car: np.ndarray
    x: int
    y: int
    w: int
    h: int
    index: int = 0
    color: tuple = (0, 0, 0)
    direction: Direction = Direction.UNDEFINED
