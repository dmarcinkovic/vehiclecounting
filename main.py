from __future__ import print_function

import argparse
import math
import os

import cv2
import numpy as np

from Car import Car
from Car import Direction


def parse_arguments():
    parser = argparse.ArgumentParser(description='Vehicle counting system')

    parser.add_argument('--input', type=str, required=True, help='Path to the input video')
    parser.add_argument('--output', type=str, required=True, help='Path to the output video')
    return parser.parse_args()


def extract_first_frame(args):
    if not os.path.exists(args.input):
        print("[ERROR] Input file {} does not exist.".format(args.input))
        return None

    video = cv2.VideoCapture(args.input)

    if not video.isOpened():
        print("[ERROR] Could not open {}".format(args.input))
        return None

    ret, first_frame = video.read()

    if first_frame is None:
        print("[ERROR] Could not read the first frame of a video")
        return None

    return first_frame


def blur_image(image):
    blur = cv2.blur(image, (5, 5))
    blur = cv2.medianBlur(blur, 5)
    blur = cv2.GaussianBlur(blur, (5, 5), 0)
    blur = cv2.bilateralFilter(blur, 9, 75, 75)

    return blur


def get_mask(image):
    image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    light_grey = (120, 10, 30)
    dark_grey = (220, 90, 120)

    mask = cv2.inRange(image_hsv, light_grey, dark_grey)
    return mask


def road_segmentation(args):
    first_frame = extract_first_frame(args)

    if first_frame is None:
        return

    blur = blur_image(first_frame)

    mask = get_mask(blur)

    cv2.imshow('First frame', first_frame)
    cv2.imshow('Segmented image', mask)
    cv2.waitKey()


def draw_frame_info(frame, video):
    cv2.rectangle(frame, (10, 2), (100, 20), (255, 255, 255), -1)
    cv2.putText(frame, str(video.get(cv2.CAP_PROP_POS_FRAMES)), (15, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                (0, 0, 0))


def draw_car_count(car_count, frame):
    text = "Number of cars {}".format(car_count)
    cv2.putText(frame, text, (80, 20), cv2.FONT_HERSHEY_COMPLEX, 0.5, color=(0, 0, 255))


def label_car(img, car):
    cv2.rectangle(img, pt1=(car.x, car.y), pt2=(car.x + car.w, car.y + car.h), color=car.color,
                  thickness=2)
    text = "Car {}".format(car.index)
    cv2.putText(img, text, (car.x, car.y - 5), cv2.FONT_HERSHEY_COMPLEX, 0.5, color=(255, 0, 0))


def initialize_kalman():
    kalman = cv2.KalmanFilter(4, 2)
    kalman.measurementMatrix = np.array([[1, 0, 0, 0], [0, 1, 0, 0]], np.float32)
    kalman.transitionMatrix = np.array([[1, 0, 1, 0], [0, 1, 0, 1],
                                        [0, 0, 1, 0], [0, 0, 0, 1]], np.float32)

    kalman.processNoiseCov = np.array([[1, 0, 0, 0],
                                       [0, 1, 0, 0],
                                       [0, 0, 1, 0],
                                       [0, 0, 0, 1]], np.float32) * 0.03
    return kalman


class Tracker(object):

    def __init__(self, car: Car):
        self.kalman = initialize_kalman()
        self.car = car
        self.initial_position = (car.x, car.y)

    def update(self, box):
        prediction = self.kalman.predict()

        x, y, w, h = box
        self.car.x = int(prediction[0] + self.initial_position[0])
        self.car.y = int(prediction[1] + self.initial_position[1])
        self.car.w = w
        self.car.h = h

        self.kalman.correct(np.array([x - self.initial_position[0], y - self.initial_position[1]], np.float32))


def draw_line_limits(image, upper_limit, lower_limit, width, color=(0, 0, 255)):
    cv2.line(image, (0, upper_limit), (width, upper_limit), color)
    cv2.line(image, (0, lower_limit), (width, lower_limit), color)


def background_subtractions(args):
    if not os.path.exists(args.input):
        print("[ERROR] Input file {} does not exist.".format(args.input))
        return

    background_sub = cv2.createBackgroundSubtractorKNN()

    capture = cv2.VideoCapture(args.input)

    if not capture.isOpened():
        print("[ERROR] Could not open {}.".format(args.input))
        return

    print("[INFO] Perform background subtraction")

    frame_count = 0
    min_area = 500
    max_area = 5000
    start_from_frame = 20

    upper_line_limit = 0.78
    lower_line_limit = 0.4

    threshold = 70

    trackers = []
    writer = None

    colors = generate_colors(400)
    car_index = 0

    while True:
        ret, frame = capture.read()

        if frame is None:
            break

        width, height = (len(frame[0]), len(frame))
        upper_line_y = int(height * upper_line_limit)
        lower_line_y = int(height * lower_line_limit)

        frame_count += 1
        mask = background_sub.apply(frame)

        draw_frame_info(frame, capture)

        if frame_count > start_from_frame:
            background_image = cv2.dilate(mask, (11, 11), 2)
            contours, _ = cv2.findContours(background_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            boxes = []
            for contour in contours:
                area = cv2.contourArea(contour)

                if area < min_area or area > max_area:
                    continue

                x, y, w, h = cv2.boundingRect(contour)
                boxes.append((x, y, w, h))

            used_indices = {-1}
            print("Frame: ", frame_count)

            new_trackers = []
            for tracker in trackers:
                distance = float('inf')

                box_index = -1
                matching_box = None

                c = tracker.car
                if c.y > upper_line_y or c.y < lower_line_y:
                    continue

                for i, box in enumerate(boxes):
                    if i in used_indices:
                        continue

                    x, y, w, h = box
                    d = math.dist((x, y), (tracker.car.x, tracker.car.y))
                    if d < distance:
                        distance = d
                        box_index = i
                        matching_box = box

                if distance < threshold and matching_box is not None:
                    used_indices.add(box_index)
                    tracker.update(matching_box)
                    new_trackers.append(tracker)

            trackers = new_trackers.copy()

            for i, box in enumerate(boxes):
                if i in used_indices:
                    continue

                x, y, w, h = box

                if y > upper_line_y or y < lower_line_y:
                    continue

                car_image = crop_image(frame.copy(), x, y, w, h)
                car = Car(car_image, x, y, w, h)
                car.color = colors[car_index]
                car.index = car_index

                car_index += 1

                trackers.append(Tracker(car))

            for tracker in trackers:
                car = tracker.car
                label_car(frame, car)

        if writer is None:
            fourcc = cv2.VideoWriter_fourcc(*"MJPG")
            writer = cv2.VideoWriter(args.output, fourcc, 30, (width, height), True)

        draw_car_count(car_index, frame)

        draw_line_limits(frame, upper_line_y, lower_line_y, width)
        writer.write(frame)

    print("[INFO] Save output video to {}".format(args.output))


def get_yolo_labels():
    labels_path = 'resources/coco.names'
    labels = open(labels_path).read().strip().split('\n')

    return labels


def get_caffe_labels():
    labels_path = 'resources/caffe.classes'
    labels = open(labels_path).read().strip().split('\n')

    return labels


def load_net_from_darknet():
    weights_path = 'resources/yolov3.weights'
    config_path = 'resources/yolov3.cfg'

    net = cv2.dnn.readNetFromDarknet(config_path, weights_path)

    print("[INFO] Loaded network from Darknet")

    return net


def load_net_from_caffe():
    model = 'resources/MobileNetSSD_deploy.caffemodel'
    proto_txt = 'resources/MobileNetSSD_deploy.prototxt'

    net = cv2.dnn.readNetFromCaffe(proto_txt, model)

    print("[INFO] Loaded network from Caffe")

    return net


def get_layer_names(network):
    layer_names = network.getLayerNames()
    layer_names = [layer_names[i - 1] for i in network.getUnconnectedOutLayers()]

    return layer_names


def extract_bounding_boxes(network, image, threshold):
    boxes = []
    confidences = []
    class_ids = []

    layer_names = get_layer_names(network)

    h, w = image.shape[:2]
    blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416), swapRB=True, crop=False)

    network.setInput(blob)
    layer_output = network.forward(layer_names)

    for output in layer_output:
        for detection in output:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]

            if confidence > threshold:
                box = detection[0:4] * np.array([w, h, w, h])
                (center_x, center_y, width, height) = box.astype('int')

                x = int(center_x - (width / 2))
                y = int(center_y - (height / 2))

                boxes.append([x, y, int(width), int(height)])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    return boxes, confidences, class_ids


def label_image(img, indices, boxes, confidences, class_ids, labels):
    if len(indices) > 0:
        for i in indices.flatten():
            (x, y) = (boxes[i][0], boxes[i][1])
            (w, h) = (boxes[i][2], boxes[i][3])

            cv2.rectangle(img, (x + 3, y + 3), (x + w - 6, y + h - 6), (255, 0, 0), 2)
            # text = "{}: {:.4f}".format(labels[class_ids[i]], confidences[i])
            # cv2.putText(img, text, (x, y - 5), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 0, 255))


def compare_images(img1, img2):
    scaled_image1 = cv2.resize(img1, (200, 200), interpolation=cv2.INTER_AREA)
    scaled_image2 = cv2.resize(img2, (200, 200), interpolation=cv2.INTER_AREA)

    orb = cv2.ORB_create()
    sift = cv2.SIFT_create()

    key_points1, descriptor1 = sift.detectAndCompute(scaled_image1, None)
    key_points2, descriptor2 = sift.detectAndCompute(scaled_image2, None)

    if descriptor1 is None or descriptor2 is None:
        return -1

    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(descriptor1.astype(np.float32), descriptor2.astype(np.float32), k=2)

    good_matches = []
    for m, n in matches:
        if m.distance < 0.7 * n.distance:
            good_matches.append(m)

    if len(good_matches) >= 0:
        return len(good_matches)
    else:
        return -1


def crop_image(img, x, y, width, height):
    return img[y:y + height, x:x + width]


def generate_colors(n):
    color_palette = np.random.randint(0, 255, size=(n, 3), dtype="int32")
    return [[int(c) for c in color] for color in color_palette]


def determine_direction(current_car, previous_car):
    if previous_car.direction != Direction.UNDEFINED:
        return previous_car.direction

    if current_car.y > previous_car.y:
        return Direction.FORWARD
    elif current_car.y < previous_car.y:
        return Direction.BACKWARD
    else:
        return Direction.UNDEFINED


def is_correct_label(class_id, labels):
    return labels[class_id] == "car" or labels[class_id] == "truck" or labels[class_id] == "bus"


def get_dimensions(box):
    (x, y) = (box[0], box[1])
    (w, h) = (box[2], box[3])

    if x < 0:
        x = 0

    if y < 0:
        y = 0

    return x, y, w, h


def caffe_model_object_detections(args):
    if not os.path.exists(args.input):
        print("[ERROR] Input file {} does not exist.".format(args.input))
        return

    labels = get_caffe_labels()
    net = load_net_from_caffe()

    colors = generate_colors(200)

    video = cv2.VideoCapture(args.input)

    score_threshold = 0.1
    writer = None

    frame_count = 0

    while True:
        ret, frame = video.read()
        if not ret:
            break

        frame_count += 1

        height, width = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(frame, 2.0 / 255.0, (300, 300), 255.0 / 2.0)
        net.setInput(blob)
        detections = net.forward()

        print("Frame", frame_count)

        for i in np.arange(0, detections.shape[2]):
            confidence = detections[0, 0, i, 2]
            if confidence > score_threshold:
                index = int(detections[0, 0, i, 1])
                box = detections[0, 0, i, 3:7] * np.array([width, height, width, height])

                (x1, y1, x2, y2) = box.astype("int")
                label = "{}: {:.2f}%".format(labels[index], confidence * 100)
                cv2.rectangle(frame, (x1, y1), (x2, y2), colors[index], 2)
                y = y1 - 15 if y1 - 15 > 15 else y1 + 15
                cv2.putText(frame, label, (x1, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, colors[index], 2)

        if writer is None:
            fourcc = cv2.VideoWriter_fourcc(*"MJPG")
            writer = cv2.VideoWriter(args.output, fourcc, 30, (width, height), True)

        writer.write(frame)

    print("[INFO] Save video to {}".format(args.output))


def object_detection(args):
    if not os.path.exists(args.input):
        print("[ERROR] Input file {} does not exist.".format(args.input))
        return

    colors = generate_colors(500)
    labels = get_yolo_labels()
    net = load_net_from_darknet()

    video = cv2.VideoCapture(args.input)

    score_threshold = 0.5
    nms_threshold = 0.3

    writer = None

    print("[INFO] Perform object detection on video frames")

    cars = []
    frame_count = 0
    car_index = 0
    previous_cars = []

    upper_line_limit = 0.78
    lower_line_limit = 0.5

    min_box_width = 30
    min_box_height = 30

    while True:
        ret, frame = video.read()

        if not ret:
            break

        frame_count += 1
        print("Frame ", frame_count)

        boxes, confidences, class_ids = extract_bounding_boxes(net, frame, score_threshold)
        indices = cv2.dnn.NMSBoxes(boxes, confidences, score_threshold, nms_threshold)
        label_image(frame, indices, boxes, confidences, class_ids, labels)

        width, height = (len(frame[0]), len(frame))
        upper_line_y = int(height * upper_line_limit)
        lower_line_y = int(height * lower_line_limit)

        used_cars = {-1}

        for c in previous_cars:
            best_match = -1
            best_match_car = None
            best_match_index = -1

            if c.y > upper_line_y or c.y < lower_line_y:
                continue

            if len(indices) > 0:
                for i in indices.flatten():
                    if i in used_cars:
                        continue

                    x, y, w, h = get_dimensions(boxes[i])

                    if w < min_box_width or h < min_box_height:
                        continue

                    car_image = crop_image(frame.copy(), x, y, w, h)
                    car = Car(car_image, x, y, w, h)

                    if is_correct_label(class_ids[i], labels):
                        if abs(c.x - car.x) > car.w:
                            continue
                        if abs(c.y - car.y) > 2 * car.h:
                            continue

                        common_descriptors = compare_images(car.car, c.car)

                        if common_descriptors > best_match:
                            best_match = common_descriptors
                            best_match_car = car
                            best_match_index = i

            if best_match >= 0:
                best_match_car.index = c.index
                best_match_car.color = c.color
                best_match_car.direction = determine_direction(best_match_car, c)

                cars.append(best_match_car)
                used_cars.add(best_match_index)

        if len(indices) > 0:
            for i in indices.flatten():
                if i in used_cars:
                    continue

                x, y, w, h = get_dimensions(boxes[i])

                if w < min_box_width or h < min_box_height:
                    continue

                if y > upper_line_y or y < lower_line_y:
                    continue

                if is_correct_label(class_ids[i], labels):
                    car_image = crop_image(frame.copy(), x, y, w, h)
                    car = Car(car_image, x, y, w, h)

                    car.index = car_index
                    car.color = colors[car_index]

                    cars.append(car)
                    car_index += 1

        for car in cars:
            label_car(frame, car)

        if writer is None:
            fourcc = cv2.VideoWriter_fourcc(*"MJPG")
            writer = cv2.VideoWriter(args.output, fourcc, 30, (width, height), True)

        draw_line_limits(frame, upper_line_y, lower_line_y, width)

        draw_frame_info(frame, video)
        draw_car_count(car_index, frame)

        writer.write(frame)

        previous_cars = cars.copy()
        cars = []

    print("[INFO] Save video to {}".format(args.output))


if __name__ == '__main__':
    arguments = parse_arguments()

    # object_detection(arguments)
    # road_segmentation(arguments)
    background_subtractions(arguments)
    # caffe_model_object_detections(arguments)
